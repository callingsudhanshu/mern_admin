const express = require('express');
const router = express.Router();
// const auth = require('../../middleware/auth')

//Item Model
const Item = require('../../schema/ItemSchema');

//@routes /api/items
// router.get('/', async function(req, res, next) {
    // var services = await addPage.find();
    // res.render('index', { title: 'Express', posts : services });
// });

router.get('/', (req,res) => {
    Item.find()
    .sort({date: -1})
    .then(items => res.json(items))
});

router.post('/', (req,res) => {
    const newItem = new Item({
         name: req.body.name
    });
    newItem.save()
    .then(item => res.json(item));
});

router.delete('/:id',(req,res) => {
    Item.findById(req.params.id)
    .then(item => item.remove().then(() => res.json({success : true})))
    .catch(err => res.status(404).json({success : false}));
});


module.exports = router;